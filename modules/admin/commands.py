from aiogram import types
from aiogram.dispatcher.webhook import SendMessage
from aiogram.utils.markdown import text

from core import dp
from .models import User
from .utils import sender_is_admin


@dp.message_handler(commands=['usermod'], func=sender_is_admin)
async def cmd_usermod(message: types.Message):
    args = message.get_args().split()
    if len(args) < 1:
        return

    user_id = args.pop()
    if not user_id.isdigit():
        return

    set_staff = None
    set_admin = None

    for item in args:
        status, part = get_part(item)
        if part == 'staff':
            set_staff = status
        elif part == 'admin':
            set_admin = status

    user = User.set_permissions(user_id, set_staff, set_admin)
    return SendMessage(message.from_user.id, text(
        text('User:', user_id),
        text('Staff:', user.is_staff),
        text('Admin:', user.is_admin), sep='\n'))


def get_part(part: str) -> (bool, str):
    if part.startswith('+'):
        return True, part.lstrip('+').lower()
    elif part.startswith('-'):
        return False, part.lstrip('-').lower()
    else:
        return None, None
