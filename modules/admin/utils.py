from aiogram import types

from . import models


def sender_is_staff(message: types.Message):
    return models.User.check_staff(message.from_user.id)


def sender_is_admin(message: types.Message):
    return models.User.check_admin(message.from_user.id)
