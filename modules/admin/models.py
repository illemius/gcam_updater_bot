from playhouse.postgres_ext import *

from core import config, loader
from core.db.models import BaseModelDate

root = config.get('telegram', {}).get('root', 0)


@loader.export
class User(BaseModelDate):
    user_id = IntegerField()
    is_admin = BooleanField(default=False)
    is_staff = BooleanField(default=False)

    @classmethod
    def get_user(cls, user_id) -> 'User' or None:
        try:
            user = cls.get(cls.user_id == user_id)
        except cls.DoesNotExist:
            return None
        else:
            return user

    @classmethod
    def check_staff(cls, user_id):
        if user_id == root:
            return True
        user = cls.get_user(user_id)
        return user and user.is_staff

    @classmethod
    def check_admin(cls, user_id):
        if user_id == root:
            return True
        user = cls.get_user(user_id)
        return user and (user.is_staff or user.is_admin)

    @classmethod
    def set_permissions(cls, user_id, staff=None, admin=None) -> 'User':
        user, _ = cls.get_or_create(user_id=user_id)
        if staff is not None:
            user.is_staff = staff
        if admin is not None:
            user.is_admin = admin
        user.save()
        return user
