from aiogram import types
from aiogram.dispatcher.webhook import AnswerCallbackQuery, SendMessage
from aiogram.types import ChatActions, ParseMode, ChatType

from core import bot, config, dp
from .checker import prepare_keyboard
from .models import UrlsList

stable = config.get('xda-gcam', {}).get('stable', {})

TEXT = 'Google Camera app with 1080p 60FPS video, slow motion and HDR+. By BSG and ivanich.'
# stable, latest, suggested settings
btn_stable = types.InlineKeyboardButton('Stable', url=stable['url'])
btn_settings = types.InlineKeyboardButton('Suggested settings',
                                          url='https://forum.xda-developers.com/showpost.php?p=73972732&postcount=565')


@dp.message_handler(commands=['start'], func=lambda m: m.chat.type == ChatType.PRIVATE)
@dp.message_handler(commands=['gcam'])
async def cmd_start(message: types.Message):
    await bot.send_chat_action(message.chat.id, ChatActions.TYPING)

    markup = types.InlineKeyboardMarkup(row_width=1)
    try:
        latest: UrlsList = UrlsList.get(UrlsList.selected >= 0)
    except UrlsList.DoesNotExist:
        latest_title = stable['title']
    else:
        markup.add(types.InlineKeyboardButton('Latest', url=latest.selected_url))
        latest_title = latest.selected_name
    markup.add(btn_stable, btn_settings)

    text = f"{TEXT}\n\n<b>Latest</b>: {latest_title}\n<b>Stable</b>: {stable['title']}"
    return SendMessage(message.chat.id, text, reply_markup=markup, parse_mode=ParseMode.HTML).reply(message)


@dp.callback_query_handler(func=lambda callback: callback.data.startswith('RL:'))
async def callback_query(callback: types.CallbackQuery):
    data = callback.data.split(':')
    item_id = int(data.pop())
    list_id = int(data.pop())

    if item_id < 0:
        return await bot.edit_message_text('This set of changes is canceled.',
                                           callback.message.chat.id, callback.message.message_id)
    else:
        try:
            urls_list = UrlsList.get(UrlsList.id == list_id)
        except UrlsList.DoesNotExist:
            return await bot.edit_message_text('Invalid URL\'s list!',
                                               callback.message.chat.id, callback.message.message_id)
        else:
            urls_list.selected = item_id
            urls_list.save()
            return await bot.edit_message_text(
                f"New latest version is: {urls_list.selected_name}",
                callback.message.chat.id, callback.message.message_id)


@dp.callback_query_handler(func=lambda callback: callback.data.startswith('RP:'))
async def callback_query(callback: types.CallbackQuery):
    text = callback.message.html_text.split('\n')

    data = callback.data.split(':')
    page_id = int(data.pop())
    list_id = int(data.pop())

    try:
        urls_list = UrlsList.get(UrlsList.id == list_id)
    except UrlsList.DoesNotExist:
        return AnswerCallbackQuery(callback.id, 'Something went wrong.')

    total_pages, markup = prepare_keyboard(urls_list, page_id)

    if total_pages > 1:
        text.pop()
        text.append(f"<b>Page: {page_id + 1}/{total_pages}</b>")
    await bot.edit_message_text('\n'.join(text),
                                callback.message.chat.id, callback.message.message_id,
                                reply_markup=markup, parse_mode=ParseMode.HTML)
