import hashlib
import math

import peewee
from aiogram import types
from aiogram.types import ParseMode

from core import bot, database, config
from . import log
from .models import UrlsList
from .parser import get_content, get_urls

root = config.get('telegram', {}).get('root', 0)
PER_PAGE = 10


async def check(force=False):
    log.info('Fetch URL\'s list...')
    urls_list = UrlsList()

    content = await get_content()
    checksum = hashlib.sha1()
    for url, title in get_urls(content):
        checksum.update(url.encode('utf-8'))
        urls_list.urls.append(url)
    urls_list.checksum = checksum.hexdigest()

    try:
        with database.atomic():
            urls_list.save()
    except peewee.IntegrityError:
        log.info('URL\'s list is not updated.')
        if force:
            log.warning('Force notify.')
            urls_list = UrlsList.select().where(UrlsList.checksum == checksum.hexdigest()).limit(1)
            await notify(urls_list[0])
    else:
        log.info('URL\'s list is updated!')
        urls_list = UrlsList.select().where(UrlsList.checksum == checksum.hexdigest()).limit(1)
        await notify(urls_list[0])


async def notify(urls_list: UrlsList):
    pages, markup = prepare_keyboard(urls_list)
    text = 'URL\'s list is updated!'
    if pages > 1:
        text += f"\n<b>Page: 1/{pages}</b>"
    await bot.send_message(root, text, reply_markup=markup, parse_mode=ParseMode.HTML)


def prepare_keyboard(urls_list: UrlsList, page=0):
    markup = types.InlineKeyboardMarkup(row_width=4)
    limit = PER_PAGE
    offset = page * limit
    current_page = urls_list.urls[page * limit:page * limit + limit]
    for index, url in enumerate(current_page, start=offset):
        title = url.rpartition('/')[-1].rpartition('.')[0]
        markup.add(types.InlineKeyboardButton(title, callback_data=f"RL:{urls_list.pk}:{index}"))

    pagination = []
    total_pages = math.ceil(len(urls_list.urls) / PER_PAGE)
    if total_pages > 1:
        if page > 0:
            pagination.append(
                types.InlineKeyboardButton(f"<<< First", callback_data=f"RP:{urls_list.pk}:0"))
        if page > 1:
            pagination.append(
                types.InlineKeyboardButton(f"< Previous", callback_data=f"RP:{urls_list.pk}:{page - 1}"))
        if page < total_pages - 2:
            pagination.append(
                types.InlineKeyboardButton(f"Next >", callback_data=f"RP:{urls_list.pk}:{page + 1}"))
        if page < total_pages - 1:
            pagination.append(
                types.InlineKeyboardButton(f"Last >>>", callback_data=f"RP:{urls_list.pk}:{total_pages - 1}"))
        markup.add(*pagination)
    markup.add(types.InlineKeyboardButton('Not changed', callback_data=f"RL:{urls_list.pk}:-1"))
    return total_pages, markup
