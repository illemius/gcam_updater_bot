from aiohttp import client
from bs4 import BeautifulSoup

from core import dp


async def get_page_content(session: client.ClientSession):
    async with session.get('https://forum.xda-developers.com/showpost.php?p=73446833&postcount=2&styleid=15') as req:
        return await req.text()


async def get_content():
    session = dp.bot.create_temp_session()
    try:
        return await get_page_content(dp.bot.create_temp_session())
    finally:
        dp.bot.destroy_temp_session(session)


def soup(content: str) -> BeautifulSoup:
    return BeautifulSoup(content, 'lxml')


def get_urls(content):
    page = soup(content)

    spoiler = page.find('div', attrs={'class': 'bbcode-hide'})
    for link in spoiler.find_all('a'):
        if 'https://www.celsoazevedo.com/' not in link['href']:
            continue
        yield link['href'], link.string
