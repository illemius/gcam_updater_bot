from playhouse.postgres_ext import *

from core import loader
from core.db.models import BaseModelDate


def default_list():
    return []


@loader.export
class UrlsList(BaseModelDate):
    urls = JSONField(default=default_list())
    checksum = CharField(unique=True)

    selected = IntegerField(default=-1)

    @property
    def selected_url(self):
        if self.selected >= 0:
            return self.urls[self.selected]

    @property
    def selected_name(self):
        url = self.selected_url
        if url:
            return url.rpartition('/')[-1].rpartition('.')[0]
