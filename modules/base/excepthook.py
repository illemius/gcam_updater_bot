from aiogram import types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import exceptions

from core import dp


@dp.errors_handler()
async def error_handler(dispatcher: Dispatcher, update: types.Update, exception):
    try:
        raise exception
    except exceptions.BadRequest:
        if update.callback_query:
            await dispatcher.bot.answer_callback_query(update.callback_query.id, 'Too many requests!')
