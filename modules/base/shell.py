from core import parser, shell
from core.shell import args
from .checker import check


@parser.register_command
class CheckList(shell.AsyncCommand):
    command = 'check_list'

    force = args.BoolArgument()

    async def execute(self):
        await check(self.force)
