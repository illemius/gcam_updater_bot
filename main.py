#!/usr/bin/env python3
import asyncio
import logging
import logging.config
import os
import sys

import yaml
from aiogram.utils import context

import core
from core import commands, shell

logging.basicConfig(level=logging.DEBUG)


async def reload(filename):
    logging.warning(f"File '{filename}' is changed! Reloading...")
    await asyncio.sleep(3)
    python = sys.executable
    os.execl(python, python, *sys.argv)
    sys.exit()


def load_packages():
    sys.path.insert(0, os.path.join(core.BASE_PATH, 'modules'))
    for pkg in core.MODULES:
        core.loader.load_package(pkg)


def setup_logging():
    with open(os.path.join(core.BASE_PATH, 'logging.yaml')) as f:
        config = yaml.load(f)
        logging.config.dictConfig(config)


async def main(argv):
    setup_logging()
    core.loop.set_task_factory(context.task_factory)
    load_packages()
    core.parser.register_command(commands.StartCommand)
    core.parser.register_command(commands.CheckDb)
    core.fw.handlers.append(reload)
    # core.fw.async_start_watching()
    try:
        command = core.parser.parse(argv)
        await command()
    except shell.exceptions.ParserException as e:
        sys.exit(e)


async def shutdown():
    core.dp.stop_pooling()
    core.dp.storage.close()
    await core.dp.storage.wait_closed()


if __name__ == '__main__':
    try:
        core.loop.run_until_complete(main(sys.argv[1:]))
    except KeyboardInterrupt:
        pass
    finally:
        core.loop.run_until_complete(shutdown())
