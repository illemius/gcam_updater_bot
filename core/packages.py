import importlib
import inspect
import logging
import pkgutil
import typing

log = logging.getLogger(__name__)

REQUIREMENT_SEPARATOR = '::'


class PackagesLoader:
    def __init__(self, *, debug=False):
        self._debug = debug

        self._exports = {}
        self.modules = {}

    def set_debug(self, debug: bool = True):
        """
        Enable or disable debugging.

        :param debug:
        """
        self._debug = debug

    def is_debug(self) -> bool:
        """
        Check debugging status

        :type: :obj:`bool`
        """
        return self._debug

    def __debug(self, *items, sep=' '):
        """
        Write log only if debugging is enabled.

        :param items:
        :param sep:
        :return:
        """
        if self.is_debug():
            log.debug(sep.join(map(str, items)))

    def load_package(self, package, recursive=True):
        """
        Import all submodules of a module, recursively, including subpackages

        :param package: package (name or actual module)
        :type package: str | module
        :param recursive: recursive import
        :type recursive: :obj:`bool`
        :rtype: dict[str, types.ModuleType]
        """
        if isinstance(package, str):
            package = importlib.import_module(package)

        full_name = package.__name__
        results = {full_name: package}

        for pkg_loader, name, is_pkg in pkgutil.walk_packages(package.__path__):
            full_name = package.__name__ + '.' + name
            self.modules[full_name] = results[full_name] = importlib.import_module(full_name)

            if recursive and is_pkg:
                results.update(self.load_package(full_name))
        return results

    def export_many(self, *items, location=None):
        """
        Export multiple objects.

        :param items:
        :param location:
        :return:
        """
        if not location:
            frame = inspect.currentframe().f_back
            location = inspect.getmodule(frame).__name__

        for item in items:
            self.export(item, location=location)

    def export(self, obj, *, location=None, name=None) -> typing.Any:
        """
        Export object

        :param obj:
        :param location:
        :param name:
        :return:
        """
        if isinstance(obj, Exportable):
            name = obj.get_name()
            location = obj.get_location()
            obj = obj.get_obj()
        else:
            if not location:
                frame = inspect.currentframe().f_back
                location = inspect.getmodule(frame).__name__

            if name is None:
                if hasattr(obj, '__name__'):
                    name = obj.__name__
                else:
                    name = obj.__class__.__name__

        return self._export(location, name, obj)

    def _export(self, location, name, obj):
        """
        Exporter

        :param location:
        :param name:
        :param obj:
        :return:
        """
        self.__debug(f"Export '{location}{REQUIREMENT_SEPARATOR}{name}' is {obj}")

        if location not in self._exports:
            self._exports[location] = {}

        self._exports[location][name] = obj
        return obj

    def require(self, requirement: str) -> typing.Any:
        """
        Get exported object

        :param requirement:
        :return:
        """
        location, _, name = requirement.partition(REQUIREMENT_SEPARATOR)

        try:
            module_objects = self._exports[location]
        except KeyError as e:
            raise ImportError(f"No one object exported from {e}")
        else:
            if name is None:
                return module_objects

        try:
            obj = module_objects[name]
        except KeyError as e:
            raise ImportError(f"Cannot import {e} from \"{location}\"")
        else:
            return obj

    def iter_objects(self) -> (str, str, typing.Any):
        """
        Iterate over all exported objects
        """
        for location_name, location in self._exports.items():
            for obj_name, obj in location.items():
                yield location_name, obj_name, obj


class Exportable:
    """
    Wrapper for exportable items
    """

    def __init__(self, obj, *, location=None, name=None):
        if not location:
            frame = inspect.currentframe().f_back
            location = inspect.getmodule(frame).__name__

        if name is None:

            if hasattr(obj, '__name__'):
                name = obj.__name__
            else:
                name = obj.__class__.__name__

        self._obj = obj
        self._location = location
        self._name = name

    def get_obj(self):
        return self._obj

    def get_location(self):
        return self._location

    def get_name(self):
        return self._name


loader: PackagesLoader = PackagesLoader()


def require(requirement, *, modules_loader=None) -> typing.Any:
    """
    Get exported object

    :param requirement:
    :param modules_loader:
    :return:
    """
    if modules_loader is None:
        modules_loader = loader

    return modules_loader.require(requirement)


def make_req_str(*parts, name=None) -> str:
    """
    Generate requirement string

    :param parts:
    :param name:
    :return:
    """
    requirement_str = '.'.join(parts)
    if name:
        requirement_str += REQUIREMENT_SEPARATOR + name
    return requirement_str
