import asyncio
import logging
import os

import re
import threading
import time
import typing

DEFAULT_TIMEOUT = 5
DEFAULT_IGNORE = re.compile('\.pyc$|__pycache__')

NOT_CHANGED = None
CHANGED = 1
DELETED = 2

THREADED = 1
ASYNCHRONOUS = 2

log = logging.getLogger(__name__)


class FilesWatcher:
    """
    Advanced files watcher
    """

    def __init__(self, *, timeout: int = DEFAULT_TIMEOUT):
        self.timeout = timeout

        self._watch_list = []

        self.handlers = []
        self.on_change = []

        self._watch = False

    def watch(self, path: str, *, recursive: bool = True, ignore: str = DEFAULT_IGNORE):
        """
        Watch file or directory

        :param path: file path
        :type path: :obj:`str`
        :param recursive: scan directory
        :type recursive: :obj:`bool`
        :param ignore: regexp string
        :type ignore: :obj:`str`
        :return:
        """
        file = FileObject(path)
        self._watch_list.append(file)

        if recursive and file.is_dir():
            to_be_add = []

            for root, dirs, files in os.walk(path):
                if ignore and re.search(ignore, root):
                    continue

                for dir_name in dirs:
                    dir_path = os.path.join(root, dir_name)
                    if ignore and re.search(ignore, dir_path):
                        continue
                    file = FileObject(dir_path)
                    to_be_add.append(file)

                for file_name in files:
                    file_path = os.path.join(root, file_name)
                    if ignore and re.search(ignore, file_path):
                        continue
                    file = FileObject(file_path)
                    to_be_add.append(file)

            self._watch_list.extend(to_be_add)

    def find(self, path, pattern, *, by_path=False, recursive=True):
        """
        Find files with regex in path

        :param path:
        :param pattern:
        :param by_path:
        :param recursive:
        """
        assert os.path.isdir(path), 'Only dirs allowed!'

        if not isinstance(pattern, type(DEFAULT_IGNORE)):
            pattern = re.compile(pattern)

        for root, dirs, files in os.walk(path):
            for file_name in files:
                file_path = os.path.join(root, file_name)
                if pattern.search(file_path if by_path else file_name):
                    self.watch(file_path, recursive=False)
            if not recursive:
                break

    def unwatch(self, path, *, by_filename=True, no_error=False):
        """
        Stop watching file or directory

        :param path:
        :param by_filename:
        :param no_error:
        :return:
        """
        is_re = isinstance(path, type(DEFAULT_IGNORE))

        need_unwatch = []
        for index, file in enumerate(self._watch_list):
            unwatch = False
            if not by_filename and file.path == path:
                # Equal path
                unwatch = True
            elif by_filename and os.path.split(file.path)[-1] == path:
                # Equal filename
                unwatch = True
            elif is_re and by_filename and path.search(os.path.split(file.path)[-1]):
                # Match in file name
                unwatch = True
            elif not by_filename and is_re and path.search(file.path):
                # Match in file path
                unwatch = True

            if unwatch:
                need_unwatch.append(file)
        if not need_unwatch and not no_error:
            raise ValueError('Invalid path!')
        else:
            for file in need_unwatch:
                self._unwatch_file(file)

    def unwatch_all(self):
        """
        Clear watch list
        """
        self._watch_list.clear()

    def touch(self, *path) -> str:
        """
        Create file nad watch

        :param path:
        """
        file_path = os.path.join(*path)
        if not os.path.exists(file_path):
            with open(file_path, 'a'):
                log.warning(f"Create empty file '{file_path}'.")
        self.watch(file_path, recursive=False)
        return file_path

    def start_watching(self, daemonic=True) -> threading.Thread:
        """
        Start watching thread
        Based on threading

        :return: thread
        :rtype: :obj:`threading.Thread`
        """
        self._enable_watcher(THREADED)
        thread = threading.Thread(target=self._watcher, name='FileWatcher', daemon=daemonic)
        thread.start()
        return thread

    def async_start_watching(self, loop=None) -> asyncio.Task:
        """
        Start watching task
        Based on asyncio

        :return: task
        :rtype: :obj:`asyncio.Task`
        """
        self._enable_watcher(ASYNCHRONOUS)
        if loop is None:
            loop = asyncio.get_event_loop()
        return loop.create_task(self._async_watcher())

    def stop(self):
        """
        Stop watcher
        """
        if self._watch:
            self._watch = False
        else:
            raise RuntimeError('Watcher is not started!')

    def _enable_watcher(self, mode: typing.Union[bool, int] = True):
        """
        Set watcher status or raise exception

        :raise: RuntimeError
        """
        if self._watch:
            raise RuntimeError('Watcher already started!')
        self._watch = mode

    def _watcher(self):
        try:
            log.warning(f"Started file watcher (total: {len(self._watch_list)} files and dirs).")
            while self._watch:
                self._check_changes()
                time.sleep(self.timeout)
        finally:
            log.warning(f"File watcher is stopped.")

    async def _async_watcher(self):
        try:
            log.warning(f"Started file watcher (total: {len(self._watch_list)} files and dirs).")
            while self._watch:
                await self._async_check_changes()
                await asyncio.sleep(self.timeout)
        finally:
            log.warning(f"File watcher is stopped.")

    async def _async_check_changes(self):
        changed = self._check_files()
        if changed:
            await self._async_notify(changed)
        self._reset_files_status()

    def _check_changes(self):
        """
        Check all files and trigger all handlers if any file is changed or deleted.
        """
        changed = self._check_files()
        if changed:
            self._notify(changed)
        self._reset_files_status()

    def _notify(self, changed):
        """
        Notify handlers

        :param changed:
        """
        if self.handlers:
            for handler in self.handlers:
                handler(changed)

        if self.on_change:
            for file in changed:
                for handler in self.on_change:
                    handler(file)

    async def _async_notify(self, changed):
        """
        Async notify handlers

        :param changed:
        :return:
        """
        fun = []
        if self.handlers:
            for handler in self.handlers:
                fun.append(handler(changed))

        if self.on_change:
            for file in changed:
                for handler in self.on_change:
                    fun.append(handler(file))

        if fun:
            await asyncio.gather(tuple(fun))

    def _check_files(self):
        log.debug('Check files...')

        changed = []
        for file in self._watch_list:
            if file.check():
                changed.append(file)
        return changed

    def _reset_files_status(self):
        for file in self._watch_list:
            if file.status == DELETED:
                self._unwatch_file(file)
            else:
                file.set_status(NOT_CHANGED)

    def _unwatch_file(self, file):
        self._watch_list.remove(file)
        del file


class FileObject:
    """
    Simple watcher object
    """

    def __init__(self, path):
        assert os.path.exists(path), 'File or directory is missing!'

        self.path = path

        stat = os.stat(path)
        self.created = stat.st_ctime
        self.modified = stat.st_mtime
        self.type = 'dir' if self.is_dir(True) else 'file'

        self.changed = False
        self.status = NOT_CHANGED

        log.debug(f"Watch {self.type} '{path}'.")

    def __del__(self):
        log.debug(f"Unwatch {self.type} '{self.path}'.")

    def check(self) -> int:
        """
        Check file or directory status

        :return: mode
        :rtype: :obj:`int`
        """
        status = NOT_CHANGED
        if not os.path.exists(self.path):
            status = DELETED
        else:
            stat = os.stat(self.path)
            if self.modified != stat.st_mtime:
                status = CHANGED

            self.created = stat.st_ctime
            self.modified = stat.st_mtime

        self.set_status(status)
        return status

    def is_dir(self, check=False) -> bool:
        """
        Object is directory?

        :param check: stat file
        :type check: :obj:`bool`
        :rtype: :obj:`bool`
        """
        if check:
            return os.path.isdir(self.path)
        return self.type == 'dir'

    def is_file(self, check=False) -> bool:
        """
        Object is file?

        :param check: stat file
        :type check: :obj:`bool`
        :rtype: :obj:`bool`
        """
        if check:
            return os.path.isfile(self.path)
        return not self.is_dir()

    def set_status(self, status):
        """
        Set status

        :param status: status
        :type status: :obj:`str`
        """
        self.status = status
        if status == NOT_CHANGED:
            self.changed = False
        else:
            self.changed = True

    def __str__(self):
        string = f"<File '{self.path}' is {self.type}"
        if self.changed:
            status = ['', 'changed', 'deleted'][self.status]
            string += ' ' + status
        string += '>'
        return string
