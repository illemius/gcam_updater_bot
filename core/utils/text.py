def to_snake_case(word):
    return ''.join(
        '_' + symbol if pos > 0 and symbol.isupper() else symbol
        for pos, symbol in enumerate(word.strip('_'))).lower()
