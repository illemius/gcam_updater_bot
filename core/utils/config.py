import yaml


def load_config(path):
    with open(path, 'rb') as file:
        return yaml.load(file)


def save_config(path, data):
    with open(path, 'wb') as file:
        yaml.dump(data, file, default_flow_style=False)


class Config:
    """
    Simple config API
    """

    def __init__(self, path):
        self.path = path

        self.data = {}
        self.load()

    def load(self):
        """
        Load config from file
        """
        self.data = load_config(self.path)

    def save(self):
        """
        Dump config
        """
        save_config(self.path, self.data)

    def get(self, item, default=None):
        return self.data.get(item, default)

    def set(self, key, value):
        self.data[key] = value

    def update(self, data=None, **kwargs):
        if data is None:
            data = []

        self.data.update(data, **kwargs)

    def __setitem__(self, key, value):
        self.data[key] = value

    def __delitem__(self, key):
        del self.data[key]

    def __getitem__(self, item):
        return self.data[item]
