from playhouse.postgres_ext import *


class CITextField(TextField):
    """
    Case insensitive field
    """
    db_field = 'citext'
