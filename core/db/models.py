import datetime

from peewee import Model
from playhouse.postgres_ext import DateTimeTZField

from core import database


def get_db_table_name(model: object):
    """
    Get table name from model

    :param model: instance of peewee.Model
    :return:
    """
    if hasattr(model, 'table_name'):
        name = getattr(model, 'table_name')
        if isinstance(name, str):
            return name

    name = ''.join(('_' + symbol if symbol.isupper() and pos > 1 else symbol
                    for pos, symbol in enumerate(model.__name__))).lower()
    if not name.endswith('s'):
        name += 's'
    return name


class BaseModel(Model):
    table_name = None

    @property
    def pk(self):
        return self.get_id()

    @classmethod
    def get_table_name(cls) -> str:
        return cls._meta.db_table

    class Meta:
        database = database
        db_table_func = get_db_table_name


class BaseModelDate(BaseModel):
    created_date = DateTimeTZField(default=None, null=True)
    modified_date = DateTimeTZField(default=None, null=True)

    def save(self, force_insert=False, only=None):
        if self.created_date is None:
            date = datetime.datetime.now()
            self.created_date = date
            self.modified_date = date
        else:
            self.modified_date = datetime.datetime.now()
        return super(BaseModel, self).save(force_insert, only)

    class Meta:
        order_by = ('-created_date',)
