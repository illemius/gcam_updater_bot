import hashlib
import importlib
import os

from peewee import Entity
from playhouse.migrate import Clause, PostgresqlMigrator, SQL, operation
from playhouse.postgres_ext import BooleanField, TextField

from core import database
from . import log
from .models import BaseModel, BaseModelDate
from ..packages import loader


class MultipleMigration(Exception):
    pass


class MigrationError(Exception):
    pass


class AlreadyMigrated(MigrationError):
    pass


class BadMigrationModule(MigrationError):
    pass


class DatabaseMigration(BaseModelDate):
    name = TextField()
    module_name = TextField()
    path = TextField()

    sha1 = TextField()

    completed = BooleanField(default=False)
    failed = BooleanField(default=False)

    @classmethod
    def get_migration(cls, path, module_name):
        name = os.path.basename(path).rpartition('.')[0]
        sha1 = hashlib.sha1()
        sha1.update(path)
        sha1 = sha1.hexdigest()

        try:
            migration: DatabaseMigration = cls.get(cls.module_name == module_name, cls.sha1 == sha1)
        except cls.DoesNotExist:
            return cls(name=name, sha1=sha1, module_name=module_name, path=path)
        return migration

    def mark_completed(self, cancel=False):
        self.failed = False
        if not cancel:
            self.completed = True
        self.save()

    def mark_failed(self):
        self.failed = True
        self.save()


class CustomPostgresqlMigrator(PostgresqlMigrator):
    @operation
    def change_column_type(self, table, column, new_type):
        return Clause(
            *self._alter_column(table, column),
            SQL('SET DATA TYPE'),
            SQL(new_type),
            SQL('USING'),
            Entity(column)
        )


def scan_exported():
    for location_name, obj_name, obj in loader.iter_objects():
        if issubclass(obj, BaseModel):
            yield obj


def create_tables() -> int:
    """
    Create tables if that aren't exist in database

    :return: count of created tables
    """
    available_tables = database.get_tables()
    need_create = set()

    for model in scan_exported():
        table_name = model.get_table_name()
        if table_name not in available_tables:
            need_create.add(model)

    if need_create:
        log.info('Create tables: ' + ', '.join([table.get_table_name() for table in need_create]))
        database.create_tables(need_create, True)

    return len(need_create)


def _load_migration(module_name):
    migration_module = importlib.import_module(module_name)
    path = migration_module.__file__

    if not hasattr(migration_module, 'main'):
        raise BadMigrationModule('Migration module has no attribute "main"')
    if not callable(migration_module.main):
        raise BadMigrationModule('Main function must be callable!')

    return migration_module, DatabaseMigration.get_migration(path, module_name)


def _start_migration(module_name):
    log.debug(f"Load migration '{module_name}'")

    migration_module, migration = _load_migration(module_name)

    if migration.completed:
        raise AlreadyMigrated('Migration is already used!')

    try:
        migration_module.main()
    except MultipleMigration:
        migration.mark_completed(True)
    except Exception:
        migration.mark_failed()
        raise
    else:
        migration.mark_completed()

    log.info(f"Migration '{module_name}' successful completed.")


def start_migrations(migrations_list):
    database.create_table(DatabaseMigration, True)

    for migration_name in migrations_list:
        try:
            _start_migration(migration_name)
        except AlreadyMigrated:
            continue
