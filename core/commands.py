from aiogram.bot.api import log

import core
from core import shell
from core.db.migrations import create_tables


class StartCommand(shell.AsyncCommand):
    command = 'start'

    skip_updates = shell.args.BoolArgument()
    debug = shell.args.BoolArgument()

    async def execute(self, *args, **kwargs):
        if self.debug:
            core.fw.find(core.BASE_PATH, r'.+\.py$')
        else:
            core.fw.watch('main.py')

        username = (await core.dp.bot.me).username
        log.info(f"Logged is as @{username}")
        if self.skip_updates:
            await core.dp.skip_updates()
        await core.dp.start_pooling()


class CheckDb(shell.AsyncCommand):
    command = 'check_db'

    async def execute(self, *args, **kwargs):
        create_tables()
