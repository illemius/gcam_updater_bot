import abc

from . import exceptions


class BaseArg(metaclass=abc.ABCMeta):
    """
    Base class for arguments
    """

    def __init__(self, *, name=None, aliases=None, default=None, parse_type=None, description=None, required=None,
                 positional=False):
        """
        :param name: name of argument (by default equals with property name)
        :param aliases: aliases for arg
        :param default: default value
        :param parse_type: parse type
        :param description: description for argument
        :param required: argument is required?
        :param positional: is positional argument?
        """
        if required is None and default is not None:
            required = False
        elif required is None:
            required = True
        if aliases is None:
            aliases = []
        self.name = name
        self.aliases = aliases
        self.default = default
        self.parse_type = parse_type
        self.description = description
        self.required = required
        self.positional = positional

    @property
    def name(self) -> str:
        """
        Name of argument

        :return:
        """
        return getattr(self, '_name', None)

    @name.setter
    def name(self, value):
        setattr(self, '_name', value)

    def __set_name__(self, owner, name):
        if self.name is None:
            self.name = name

    def __get__(self, instance, owner):
        return self.get_value(instance)

    @abc.abstractmethod
    def parse(self, instance, command_line):
        """
        Parse value

        if you are use value from command line you need to pop item from `command_line`

        :param instance: instance of command
        :param command_line:
        :return:
        """
        pass

    def set_value(self, instance, value):
        if self.parse_type is not None:
            try:
                value = self.parse_type(value)
            except ValueError as e:
                raise exceptions.BadArgument(e)
        instance.args_values[self.name] = value

    def get_value(self, instance):
        return instance.args_values.get(self.name, self.default)

    def validate(self, instance):
        value = self.get_value(instance)
        if self.required and value is None:
            raise exceptions.ArgRequired(f"Argument '--{self.name.replace('_', '-')}' is required!")


class Argument(BaseArg):
    """
    Simple argument
    """

    def parse(self, instance, command_line):
        if not command_line or command_line[0].startswith('-'):
            raise exceptions.BadArgument(f"Value for '{self.name}' is required!")
        self.set_value(instance, command_line.pop(0))


class BoolArgument(BaseArg):
    """
    Boolean argument.
    By default is False. Use as flag.
    """

    def __init__(self, *_, **kwargs):
        default = kwargs.pop('default', None)
        if default is None:
            default = False
        super().__init__(default=default, **kwargs)

    def parse(self, instance, command_line):
        self.set_value(instance, True)


class InvertedBoolArgument(BaseArg):
    """
    By default is true. Use as flag for set False value
    """

    def __init__(self, *_, **kwargs):
        default = kwargs.pop('default', None)
        if default is None:
            default = True
        super().__init__(default=default, **kwargs)

    def parse(self, instance, command_line):
        self.set_value(instance, False)


class IntegerArgument(Argument):
    """
    Argument will be parsed as Integer
    """

    def __init__(self, *_, **kwargs):
        parse_type = kwargs.pop('parse_type', None)
        super().__init__(parse_type=int, **kwargs)


class MultipleArgument(Argument):
    """
    You can use that argument multiple times

    >>> foo = args.MultipleArgument()

    CMD: executable start --foo Foo --foo Bar --foo Baz

    than in execute:
    >>> print(self.foo)
    <<< ['Foo', 'Bar', 'Baz']

    """

    def __init__(self, *_, **kwargs):
        default = kwargs.pop('default', [])
        assert isinstance(default, list)
        super(MultipleArgument, self).__init__(default=default, **kwargs)

    def set_value(self, instance, value):
        if self.parse_type is not None:
            try:
                value = self.parse_type(value)
            except ValueError as e:
                raise exceptions.BadArgument(e)

        if self.name not in instance.args_values:
            instance.args_values[self.name] = []
        instance.args_values[self.name].append(value)
