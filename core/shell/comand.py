import abc

from . import exceptions
from .args import BaseArg


class MetaCommand(abc.ABCMeta):
    """
    Metaclass for commands
    """

    def __new__(mcs, name, bases, namespace):
        cls = super(MetaCommand, mcs).__new__(mcs, name, bases, namespace)
        cls._args = {}
        cls._aliases = {}
        for base in bases:
            if isinstance(base, MetaCommand):
                cls._args.update(cls._args)
        for key, obj in namespace.items():
            if isinstance(obj, BaseArg):
                cls._args[obj.name] = obj
        for name, obj in cls._args.items():
            for alias in obj.aliases:
                cls._aliases[alias] = name
        return cls


def prepare_arg_name(arg):
    """
    Clean argument name

    :param arg:
    :return:
    """
    if arg.startswith('--'):
        double = True
    elif arg.startswith('-'):
        double = False
    else:
        return None, None

    arg = arg.lstrip('-').lower().replace('-', '_')
    return double, arg


class Command(metaclass=MetaCommand):
    """
    Abstract class for commands
    """

    command = None

    @property
    def args_values(self):
        """
        Values of arguments
        :return:
        """
        if not hasattr(self, '_args_values'):
            setattr(self, '_args_values', {})
        return getattr(self, '_args_values')

    @classmethod
    def get_command(cls):
        """
        Command line

        :return:
        """
        return cls.command if cls.command else cls.__name__.lower()

    @abc.abstractmethod
    def execute(self, *args, **kwargs):
        """
        You need to override that method

        :return:
        """
        pass

    def parse(self, command_line):
        """
        Parse command line onto command arguments

        :param command_line:
        :return:
        """
        if not command_line:
            return False

        positional = [obj for obj in self._args.values() if obj.positional]

        while command_line:
            part = command_line.pop(0)
            double_prefix, name = prepare_arg_name(part)
            if name and double_prefix and name in self._args:
                arg = self._args[name]
            elif name and part in self._aliases:
                arg = self._args[self._aliases[part]]
            elif not part.startswith('-') and positional:
                command_line.insert(0, part)
                arg = positional.pop(0)
            else:
                raise exceptions.BadArgument(f"Unknown argument '{part}'")
            arg.parse(self, command_line)
            if arg in positional:
                positional.remove(arg)

    def self_check(self):
        """
        Validate defined arguments

        :return:
        """
        for arg in self._args.values():
            arg.validate(self)

    def _prepare(self, argv):
        """
        Prepare argument

        :param argv:
        :return:
        """
        self.parse(argv)
        self.self_check()

    def start(self, argv, *args, **kwargs):
        """
        Parse and eecute command

        :param argv:
        :return:
        """
        self._prepare(argv)
        return self.execute(*args, **kwargs)


class AsyncCommand(Command):
    """
    Command for asynchronous executors
    """

    @abc.abstractmethod
    async def execute(self, *args, **kwargs):
        pass

    async def start(self, argv, *args, **kwargs):
        self._prepare(argv)
        return await self.execute(*args, **kwargs)
