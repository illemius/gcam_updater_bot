import functools
import sys

from . import exceptions
from .comand import Command


class ArgParser:
    """
    Simple args parser.
    Provide syntax: executable [command] [(args) ...] [(positional args) ...]
    """

    def __init__(self):
        self._commands = {}

    def register_command(self, command):
        """
        Register command

        :param command: subclass of Command
        :return:
        """
        assert issubclass(command, Command)
        self._commands[command.get_command()] = command
        return command

    def parse(self, argv=None):
        """
        Parse args list

        :param argv:
        :return:
        """
        if argv is None:
            argv = sys.argv[1:]

        if not argv:
            raise exceptions.BadCommand('Command is not specified!')

        command = argv.pop(0)
        if command not in self._commands:
            raise exceptions.BadCommand(f"Unknown command '{command}'!")

        command_executor = self._commands[command]()
        return functools.partial(command_executor.start, argv)
