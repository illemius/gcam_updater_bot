class ParserException(Exception):
    pass


class BadCommand(ParserException):
    pass


class BadArgument(ParserException):
    pass


class ValidationError(ParserException):
    pass


class ArgRequired(ValidationError):
    pass
