import asyncio
import os

from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from playhouse.postgres_ext import PostgresqlExtDatabase

from core.utils.watcher import FilesWatcher
from . import shell
from .packages import loader
from .utils.config import Config

BASE_PATH = os.path.dirname(os.path.dirname(__file__))
os.chdir(BASE_PATH)

loop = asyncio.get_event_loop()
parser = shell.ArgParser()
config = Config(os.path.join(BASE_PATH, 'config.yaml'))
bot: Bot = Bot(config.get('telegram', {}).get('token'), loop=loop)
dp: Dispatcher = Dispatcher(bot)
database = PostgresqlExtDatabase(**config.get('database', {}))
fw = FilesWatcher(timeout=2)

MODULES = [
    'admin',
    'base'
]
